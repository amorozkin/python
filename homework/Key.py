def key_function(array):
    new_array = {v: k for k, v in array.items()}
    return new_array

print(key_function({'a': 1, 'b': 2}))

# я не мог написать иначе, увидев решение на stackoverflow, но я понял)